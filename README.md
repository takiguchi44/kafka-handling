# Kafka
## Commands
### Launch CMD Producer
```bash
$ kafka-console-producer.sh --broker-list localhost:9092 --topic test
```

### Launch CMD Consumer
```bash
$ kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test
```