package org.takiguchi.example.kafkaproducer.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.takiguchi.example.kafkaproducer.service.KafkaProducerService;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestController
public class KafkaProducerController {
    private final KafkaProducerService kafkaProducerService;

    public KafkaProducerController(KafkaProducerService kafkaProducerService) {
        this.kafkaProducerService = kafkaProducerService;
    }

    @PostMapping("/messages")
    public ResponseEntity<String> sendMessage(@RequestBody String message) {
        ResponseEntity<String> response;
        try {
            kafkaProducerService.sendMessage(message);
            response = ResponseEntity.ok("Message sent.");
        } catch(Exception ex) {
            response = ResponseEntity.status(INTERNAL_SERVER_ERROR).body("Message no sent.");
        }
        return response;
    }
}
